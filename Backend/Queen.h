#pragma once
#include "Solider.h"
#include "Bishop.h"
#include "Rook.h"


class Queen : public Solider
{
public:
	Queen(bool color, Position &position, Board& board);
	virtual ~Queen();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
};