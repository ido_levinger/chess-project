#pragma once
#include "Solider.h"


class Rook : public Solider
{
public:
	Rook(bool color, Position& position, Board& board);
	virtual ~Rook();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
};