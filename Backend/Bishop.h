#pragma once
#include "Solider.h"


class Bishop : public Solider
{
public:
	Bishop(bool color, Position& position, Board& board);
	virtual ~Bishop();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
};