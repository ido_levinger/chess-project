#include "Queen.h"


Queen::Queen(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
}

Queen::~Queen()
{

}


bool Queen::isMoveValid(Position dest)
{
	Bishop bishop(_color, _position, _board);
	Rook rook(_color, _position, _board);

	if (bishop.isMoveValid(dest) || rook.isMoveValid(dest))
	{
		return true;
	}
	else
	{
		return false;
	}
}


char Queen::getType()
{
	return _color ? 'q' : 'Q';
}