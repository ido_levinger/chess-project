#include "Rook.h"


Rook::Rook(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
}

Rook::~Rook()
{

}


bool Rook::isMoveValid(Position dest)
{
	Position square = _position;
	int posX = _position.getX();
	int dstX = dest.getX();
	int posY = _position.getY();
	int dstY = dest.getY();
	int numOfSquares = 0;
	int i = 0;

	if ((_position.getX() == dest.getX() && _position.getY() != dest.getY()) || (_position.getY() == dest.getY() && _position.getX() != dest.getX())) // Rook is moving only in strate lines 
	{
		if (_position.getX() != dest.getX()) // if moving on X axis
		{
			numOfSquares = abs(posX - dstX);
			for (i = 0; i < numOfSquares - 1; i++)
			{
				if (posX > dstX)
				{
					square[LETTER]--;
				}
				else
				{
					square[LETTER]++;;
				}
				if (_board.isThereSoliderInPosition(square))
				{
					return false;
				}
			}
			return true;
		}
		else // if moving on Y axis
		{
			numOfSquares = abs(posY - dstY);
			for (i = 0; i < numOfSquares - 1; i++)
			{
				if (posY > dstY)
				{
					square[NUMBER]--;
				}
				else
				{
					square[NUMBER]++;
				}
				if (_board.isThereSoliderInPosition(square))
				{
					return false;
				}
			}
			return true;
		}
	}
	return false;
}


char Rook::getType()
{
	return _color ? 'R' : 'r';
}