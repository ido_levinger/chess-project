#pragma once
#include <vector>
#include <thread>
#include <string>
#include "Solider.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"
#include "Board.h"
#include "..//Frontend/Pipe.h"


class Manager
{
public:
	Manager();
	~Manager();

	void startGame();

private:
	// functions:
	int doAction();
	bool connectPipe();
	void setVector();
	void getBoard();

	bool isMoveValid(Position& src, Position& dest);
	Position returnSource();
	Position returnDest();

	// variables:
	Board _board;
	std::string _nextAction;
	std::string _msgToFrontend;
	Pipe _pipe;
	bool _whosTurn;
};