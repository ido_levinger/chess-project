#include "King.h"


King::King(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
}

King::~King()
{

}


bool King::isMoveValid(Position dest)
{
	if ((dest.getX() - _position.getX() <= 1 && dest.getX() - _position.getX() >= -1) && (dest.getY() - _position.getY() <= 1 && dest.getY() - _position.getY() >= -1)) // Valid move
	{
		return true;
	}
	else
	{
		return false;
	}
}


char King::getType()
{
	return _color ? 'k' : 'K';
}