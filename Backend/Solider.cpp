#include "Solider.h"


//Solider class:
Solider::Solider(Board& board) : _board(board)
{
	_position = "00";
}

Solider::~Solider()
{

}


void Solider::move(Position& dest)
{
	_position = dest;
}


Position& Solider::getPosition()
{
	return _position;
}


bool Solider::getColor()
{
	return _color;
}


//Position class:
Position::Position()
{
	_position[0] = '0';
	_position[1] = '0';
	_position[2] = NULL;
}


Position::Position(const char pos[POSITION_SIZE])
{
	_position[0] = pos[0];
	_position[1] = pos[1];
	_position[2] = NULL;
}


Position::Position(unsigned int x, unsigned int y)
{
	changePositionXY(x, y);
	_position[2] = NULL;
}


void Position::changePositionXY(unsigned int x, unsigned int y)
{
	_position[0] = x + 'a' - 1;
	_position[1] = y + '0';
}


int Position::getX()
{
	return (int)(_position[LETTER] - 'a' + 1); // a = 1, b = 2 ........
}


int Position::getY()
{
	return (int)(_position[NUMBER] - '0'); // '1' = 1, 2 = 2 ........
}


Position& Position::operator=(const char pos[POSITION_SIZE])
{
	_position[0] = pos[0];
	_position[1] = pos[1];
	_position[2] = pos[2];

	return *this;
}


Position& Position::operator++()
{
	if (*this == "00")
	{
		*this = "a1";
	}

	if (*this == "h8") // out of board
	{
		*this = "a1";
	}

	if (_position[0] == 'h')
	{
		_position[0] = 'a';
		_position[1]++;
	}
	else
	{
		_position[0]++;
	}

	return *this;
}


Position Position::operator++(int)
{
	operator++();
	return *this;
}


char& Position::operator[](const int index)
{
	if (index < 0 || index >= POSITION_SIZE)
		throw outOfRangeExeption();

	return _position[index];
}


const char& Position::operator[](const int index) const
{
	if (index < 0 || index >= POSITION_SIZE)
		throw outOfRangeExeption();

	return _position[index];
}


bool Position::operator==(const Position& right)
{
	int i = 0;
	
	for (i = 0; i < POSITION_SIZE; i++)
	{
		if (!(_position[i] == right[i]))
		{
			return false;
		}
	}
	return true;
}