#define _CRT_SECURE_NO_WARNINGS
#include "Manager.h"

#define ACTION_SIZE 5 // 4 for action 1 for NULL
#define MSG_SIZE 2

#define VALID_MOVE					"0" 
#define CHESS_ON_OPPONENT			"1" // TODO
#define SRC_IS_EMPTY				"2"
#define SOLIDER_OF_PLAYER_IN_DST	"3"
#define CHESS_ON_MYSELF				"4" // TODO
#define OUT_OF_BOARD				"5"
#define INVALID_MOVE_IF_SOLIDER		"6" 
#define SRC_AND_DST_SAME			"7"
#define MATE						"8"


Manager::Manager()
{
	srand(time_t(NULL));
	_whosTurn = false;
}

Manager::~Manager()
{

}


bool Manager::connectPipe()
{
	bool isConnected = _pipe.connect();
	string ans;

	while (!isConnected)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			isConnected = _pipe.connect();
		}
		else
		{
			_pipe.close();
			return isConnected; // false.
		}
	}

	return isConnected;
}


void Manager::startGame()
{
	if (!connectPipe())
		throw couldNotConnectExeption();

	getBoard();
	_pipe.sendMessageToGraphics(_board._boardStr.c_str()); // send the board string
	setVector();

	while (_nextAction != "quit")
	{
		do
		{
			_nextAction = _pipe.getMessageFromGraphics();
		}
		while (_nextAction == "");
		doAction();
		_pipe.sendMessageToGraphics(_msgToFrontend.c_str());
	}

	_pipe.close();
}


void Manager::getBoard() // this function should give a choice of game mode an then return the starting state which is sent to the frontend one time only.
{
	_board._boardStr = "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"; // default mode.
}


int Manager::doAction()
{
	Position src = returnSource();
	Position dst = returnDest();

	if (isMoveValid(src, dst)) // is move valid by poistions on board
	{
		if (_board.getSoliderByPosition(src).isMoveValid(dst)) // is move valid by solider's abillity 
		{
			_msgToFrontend = VALID_MOVE;
			
			if (_board.getSoliderByPosition(src).getType() == 'k' || _board.getSoliderByPosition(src).getType() == 'K')
			{
				_board.getSoliderByPosition(src).move(dst);
				if (_board.isChess(!_whosTurn))  // check if move anable for opponent to make chess
				{
					_msgToFrontend = CHESS_ON_MYSELF;
					_board.getSoliderByPosition(dst).move(src);
					return 0;
				}
				_board.getSoliderByPosition(dst).move(src);
			}

			if (_board.isThereSoliderInPosition(dst))
			{
				try
				{
					_board.eat(dst);
				}
				catch (gameOver& message)
				{
					std::cout << message.what() << std::endl;
					_msgToFrontend = MATE;
					return 0;
				}

			}

			_board.getSoliderByPosition(src).move(dst);
			if (_board.isChess(_whosTurn)) // check if chess was made.
			{
				_msgToFrontend = CHESS_ON_OPPONENT;
			}
			
			_whosTurn = !_whosTurn;
		}
		else
		{
			_msgToFrontend = INVALID_MOVE_IF_SOLIDER;
		}
	}

	return 0;
}


bool Manager::isMoveValid(Position& src, Position& dst)
{
	try
	{
		if (_board.getSoliderByPosition(src).getColor() != _whosTurn)
		{
			_msgToFrontend = INVALID_MOVE_IF_SOLIDER;
			return false;
		}
	}
	catch (notFoundExeption&)
	{
		_msgToFrontend = SRC_IS_EMPTY;
		return false;
	}

	if ((src[LETTER] < 'a' || src[LETTER] > 'h' || src[NUMBER] < '1' || src[NUMBER] > '8') || (dst[LETTER] < 'a' || dst[LETTER] > 'h' || dst[NUMBER] < '1' || dst[NUMBER] > '8')) // check if position on board
	{
		_msgToFrontend = OUT_OF_BOARD;
		return false;
	}
	else if (!(_board.isThereSoliderInPosition(src))) // src position is empty
	{
		_msgToFrontend = SRC_IS_EMPTY;
		return false;
	}
	else if (src == dst) // src and dst position are same
	{
		_msgToFrontend = SRC_AND_DST_SAME;
		return false;
	}
	else if (_board.isThereSoliderInPosition(dst))
	{
		if (_board.getSoliderByPosition(dst).getColor() == _board.getSoliderByPosition(src).getColor()) // solider from same color in dest
		{
			_msgToFrontend = SOLIDER_OF_PLAYER_IN_DST;
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}


void Manager::setVector()
{
	int i = 0;
	Position PositionOnBoard = "a1";

	for (i = 0; i < BOARD_SIZE; i++, PositionOnBoard++)
	{
		switch (_board._boardStr[i])
		{
			case 'K':
			{
				_board._soliders.push_back(new King(true, PositionOnBoard, _board));
				break;
			}
			case 'k':
			{
				_board._soliders.push_back(new King(false, PositionOnBoard, _board));
				break;
			}
			case 'Q':
			{
				_board._soliders.push_back(new Queen(true, PositionOnBoard, _board));
				break;
			}
			case 'q':
			{
				_board._soliders.push_back(new Queen(false, PositionOnBoard, _board));
				break;
			}
			case 'R':
			{
				_board._soliders.push_back(new Rook(true, PositionOnBoard, _board));
				break;
			}
			case 'r':
			{
				_board._soliders.push_back(new Rook(false, PositionOnBoard, _board));
				break;
			}
			case 'N':
			{
				_board._soliders.push_back(new Knight(true, PositionOnBoard, _board));
				break;
			}
			case 'n':
			{
				_board._soliders.push_back(new Knight(false, PositionOnBoard, _board));
				break;
			}
			case 'B':
			{
				_board._soliders.push_back(new Bishop(true, PositionOnBoard, _board));
				break;
			}
			case 'b':
			{
				_board._soliders.push_back(new Bishop(false, PositionOnBoard, _board));
				break;
			}
			case 'P':
			{
				_board._soliders.push_back(new Pawn(true, PositionOnBoard, _board));
				break;
			}
			case 'p':
			{
				_board._soliders.push_back(new Pawn(false, PositionOnBoard, _board));
				break;
			}
			default: // skip ('#')
			{
				break;
			}
		}
	}
}


Position Manager::returnSource()
{
	char temp[3];

	temp[0] = _nextAction[0];
	temp[1] = _nextAction[1];
	temp[2] = NULL;
	return temp;
}


Position Manager::returnDest()
{
	char temp[3];

	temp[0] = _nextAction[2];
	temp[1] = _nextAction[3];
	temp[2] = NULL;
	return temp;
}