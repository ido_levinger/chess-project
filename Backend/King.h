#pragma once
#include "Solider.h"


class King : public Solider
{
public:
	King(bool color, Position& position, Board& board);
	virtual ~King();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
};