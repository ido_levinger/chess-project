#include "Bishop.h"


Bishop::Bishop(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
}

Bishop::~Bishop()
{

}


bool Bishop::isMoveValid(Position dest)
{
	if (abs(_position.getX() - dest.getX()) == abs(_position.getY() - dest.getY())) // Bishop is moving in diagonally
	{
		Position square = _position;
		int i = 0;
		int numOfSquares = abs(_position.getY() - dest.getY());
		for (i = 0; i < numOfSquares-1; i++) // all squares in the way, but not dest
		{
			if (_position.getY() > dest.getY()) // move backward (if White)
			{
				if (_position.getX() > dest.getX())  // move left (if White)
				{
					square[LETTER]--; // X
					square[NUMBER]--; // Y
				}
				else // move right (if White)
				{
					square[LETTER]++; // X
					square[NUMBER]--; // Y
				}
			}
			else // move forward (if White)
			{
				if (_position.getX() > dest.getX())  // move left (if White)
				{
					square[LETTER]--; // X
					square[NUMBER]++; // Y
				}
				else // move right (if White)
				{
					square[LETTER]++; // X
					square[NUMBER]++; // Y
				}
			}

			if (_board.isThereSoliderInPosition(square))
			{
				return false;
			}
		}
		return true;
	}
	return false;
}


char Bishop::getType()
{
	return _color ? 'b' : 'B';
}