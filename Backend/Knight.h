#pragma once
#include "Solider.h"


class Knight : public Solider
{
public:
	Knight(bool color, Position& position, Board& board);
	virtual ~Knight();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
};