#pragma once
#include "Solider.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "Knight.h"


class Pawn : public Solider
{
public:
	Pawn(bool color, Position& position, Board& board);
	virtual ~Pawn();

	virtual bool isMoveValid(Position dest);
	virtual char getType();
	virtual void move(Position& dest);

	void pawnPromotion();

private:
	bool _isFirstMove;
};