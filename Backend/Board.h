#pragma once
#include <vector>
#include "Solider.h"

#define KING_NUM_OF_POSSIBLE_MOVES 8
#define BOARD_SIZE 64

class Position;
class Solider;
class outOfRangeExeption;

class Board
{
public:
	Board();
	~Board();

	void eat(Position& PositionToEat);
	void deleteVector();

	Solider& getSoliderByPosition(const Position& PositionOnBoard);
	Position getSoliderPositionByType(const char type); // Should not be used on other than King and Queen (because there is more than one of the others)
	unsigned int getSoliderIndexByPosition(const Position& PositionOnBoard);

	bool isThereSoliderInPosition(Position& pos);
	bool isThereSoliderInPosition(int x, int y);

	bool isChess(bool whosTurn);
	bool isMate(bool whosTurn);

	std::vector<Solider*> _soliders;
	std::string _boardStr;
};

class notFoundExeption : public std::exception
{
public:
	virtual const char* what() const
	{
		return "Solider not found!";
	}
};

class gameOver : public std::exception
{
public:
	virtual const char* what() const
	{
		return "player won!";
	}
};