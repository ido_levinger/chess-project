#include "Knight.h"


Knight::Knight(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
}

Knight::~Knight()
{

}


bool Knight::isMoveValid(Position dest)
{
	if ((abs(dest.getX() - _position.getX()) == 1 && abs(dest.getY() - _position.getY()) == 2) || (abs(dest.getX() - _position.getX()) == 2 && abs(dest.getY() - _position.getY()) == 1))
	{
		return true;
	}
	else
	{
		return false;
	}
}


char Knight::getType()
{
	return _color ? 'n' : 'N';
}