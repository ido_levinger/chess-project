#include "Board.h"

Board::Board()
{

}

Board::~Board()
{
	deleteVector();
}


Solider& Board::getSoliderByPosition(const Position& PositionOnBoard)
{
	int i = 0;

	for (i = 0; i < _soliders.size(); i++)
	{
		if (_soliders[i]->getPosition() == PositionOnBoard)
		{
			return *_soliders[i];
		}
	}
	throw notFoundExeption(); // if not found exeption will be thrown.
}


Position Board::getSoliderPositionByType(const char type)
{
	size_t i = 0;
	size_t size = _soliders.size();

	for (i = 0; i < size; i++)
	{
		if (_soliders[i]->getType() == type)
		{
			return _soliders[i]->getPosition();
		}
	}
	throw notFoundExeption();
}


unsigned int Board::getSoliderIndexByPosition(const Position& PositionOnBoard)
{
	int i = 0;

	for (i = 0; i < _soliders.size(); i++)
	{
		if (_soliders[i]->getPosition() == PositionOnBoard)
		{
			return i;
		}
	}
	throw notFoundExeption();
}


bool Board::isThereSoliderInPosition(Position& pos)
{
	try
	{
		getSoliderByPosition(pos);
		return true;
	}
	catch (notFoundExeption&)
	{
		return false;
	}
}


bool Board::isThereSoliderInPosition(int x, int y)
{
	Position temp(x, y);
	return isThereSoliderInPosition(temp);
}


void Board::eat(Position& PositionToEat)
{
	size_t size = _soliders.size();
	size_t i = 0;
	bool isKing = false;

	for (i = 0; i < size; i++)
	{
		if (_soliders[i]->getPosition() == PositionToEat)
		{
			if (_soliders[i]->getType() == 'k' || _soliders[i]->getType() == 'K')
			{
				isKing = true;
			}
			_soliders[i]->~Solider();
			_soliders.erase(_soliders.begin() + i);
			if (isKing)
			{
				throw gameOver(); // MATE
			}
			break;
		}
	}
}


bool Board::isChess(bool whosTurn)
{
	size_t i = 0;
	size_t size = _soliders.size();
	Position kingsPosition = getSoliderPositionByType(whosTurn ? 'K' : 'k');

	for (i = 0; i < size; i++)
	{
		if (_soliders[i]->getColor() == whosTurn && _soliders[i]->isMoveValid(kingsPosition))
		{
			return true;
		}
	}
	return false;
}


bool Board::isMate(bool whosTurn)
{
	Solider& king = getSoliderByPosition(getSoliderPositionByType(whosTurn ? 'K' : 'k'));
	Position kingOriginPosition = king.getPosition();
	Position positionToCheck;
	size_t i = 0;
	size_t j = 0;
	size_t counter = 0; // need to reach 8 for mate. (KING_NUM_OF_POSSIBLE_MOVES)
	Position sourcePosition;

	for (i = 0; i < KING_NUM_OF_POSSIBLE_MOVES; i++)
	{
		switch (i)
		{
			case 0:
			{
				positionToCheck.changePositionXY(king.getPosition().getX(), king.getPosition().getY() - 1); // 1 position
				break;
			}
			case 1:
			{
				positionToCheck.changePositionXY(king.getPosition().getX(), king.getPosition().getY() + 1); // 2 position
				break;
			}
			case 3:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() + 1, king.getPosition().getY() - 1); // 3 position
				break;
			}
			case 4:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() + 1, king.getPosition().getY()); // 4 position
				break;
			}
			case 5:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() + 1, king.getPosition().getY() + 1); // 5 position
				break;
			}
			case 6:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() - 1, king.getPosition().getY() - 1); // 6 position
				break;
			}
			case 7:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() - 1, king.getPosition().getY()); // 7 position
				break;
			}
			case 8:
			{
				positionToCheck.changePositionXY(king.getPosition().getX() - 1, king.getPosition().getY() + 1); // 8 position
				break;
			}
		}

		if (!(positionToCheck[LETTER] < 'a' || positionToCheck[LETTER] > 'h' || positionToCheck[NUMBER] < '1' || positionToCheck[NUMBER] > '8')) // check if position on board
		{
			try
			{
				if (getSoliderByPosition(positionToCheck).getColor() == king.getColor()) // solider from same color in positionToCheck
				{
					counter++;
				}
				else
				{
					king.move(positionToCheck);
					if (isChess(whosTurn))
					{
						counter++;
					}
					king.move(kingOriginPosition);
				}
			}
			catch (notFoundExeption&)
			{
				king.move(positionToCheck);
				if (isChess(whosTurn))
				{
					counter++;
				}
				king.move(kingOriginPosition);
			}
		}
		else
		{
			counter++;
		}
	}

	if (counter == KING_NUM_OF_POSSIBLE_MOVES)
	{
		for (i = 0; i < _soliders.size(); i++)
		{
			if (_soliders[i]->getColor() == !whosTurn)
			{
				for (j = 0, positionToCheck = "a1"; j < BOARD_SIZE; j++, positionToCheck++)
				{
					if (_soliders[i]->isMoveValid(positionToCheck))
					{
						sourcePosition = _soliders[i]->getPosition();
						_soliders[i]->move(positionToCheck);
						if (!isChess(whosTurn))
						{
							_soliders[i]->move(sourcePosition);
							return false;
						}
						_soliders[i]->move(sourcePosition);
					}
				}
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}


void Board::deleteVector()
{
	size_t i = 0;
	size_t size = _soliders.size();
	
	for (i = size-1; i > 0; i--)
	{
		_soliders[i]->~Solider();
		_soliders.erase(_soliders.begin()+i);
	}
	_soliders.clear();
}