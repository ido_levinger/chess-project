#include "Pawn.h"

#define END_OF_BOARD_WHITE 8
#define END_OF_BOARD_BLACK 1


Pawn::Pawn(bool color, Position& position, Board& board) : Solider::Solider(board)
{
	_color = color;
	_position = position;
	_isFirstMove = true;
}

Pawn::~Pawn()
{

}


bool Pawn::isMoveValid(Position dest)
{
	int colorOperator = _color ? -1 : 1;

	if (_position.getX() == dest.getX() && !_board.isThereSoliderInPosition(_position.getX(), _position.getY() + 1*colorOperator)) // regular move on X.
	{
		if (_isFirstMove && dest.getY() - _position.getY() == 2*colorOperator && !_board.isThereSoliderInPosition(_position.getX(), _position.getY() + 2*colorOperator)) // first move allows tow steps forward.
		{
			_isFirstMove = false;
			return true;
		}
		else if (dest.getY() - _position.getY() == 1*colorOperator) // one step forward.
		{
			_isFirstMove = false;
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (_board.isThereSoliderInPosition(dest) && (_position.getX() - dest.getX() == 1 || _position.getX() - dest.getX() == -1) && dest.getY() - _position.getY() == 1*colorOperator) // eat.
	{
		_isFirstMove = false;
		return true;
	}
	else
	{
		return false;
	}
}


char Pawn::getType()
{
	return _color ? 'p' : 'P';
}


void Pawn::move(Position& dest)
{
	_position = dest;
	if ((!_color && _position.getY() == END_OF_BOARD_WHITE) || (_color && _position.getY() == END_OF_BOARD_BLACK))
	{
		pawnPromotion();
	}
}


void Pawn::pawnPromotion()
{
	char newSolider = NULL;
	unsigned int index = _board.getSoliderIndexByPosition(_position);

	do
	{
		std::cout << "Enter A new solider instead of pawn:\nB - bishop, K - knight, Q - queen, R - rook\n";
		std::cin >> newSolider; 
		if (std::cin.get() != '\n') // clean cin if entred more then 1 character
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	} 
	while (newSolider != 'B' && newSolider != 'K' && newSolider != 'Q' && newSolider != 'R' && newSolider != 'b' && newSolider != 'k' && newSolider != 'q' && newSolider != 'r'); //work?
	
	_board._soliders[index]->~Solider();
	switch (newSolider)
	{
		case 'B':
		{
			_board._soliders[index] = new Bishop(_color, _position, _board);
			break;
		}
		case 'K':
		{
			_board._soliders[index] = new Knight(_color, _position, _board);
			break;
		}
		case 'Q':
		{
			_board._soliders[index] = new Queen(_color, _position, _board);
			break;
		}
		case 'R':
		{
			_board._soliders[index] = new Rook(_color, _position, _board);
			break;
		}
		case 'b':
		{
			_board._soliders[index] = new Bishop(_color, _position, _board);
			break;
		}
		case 'k':
		{
			_board._soliders[index] = new Knight(_color, _position, _board);
			break;
		}
		case 'q':
		{
			_board._soliders[index] = new Queen(_color, _position, _board);
			break;
		}
		case 'r':
		{
			_board._soliders[index] = new Rook(_color, _position, _board);
			break;
		}
	}
}