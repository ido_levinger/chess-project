#pragma once
#include <iostream>
#include <vector>
#include "Board.h"

#define POSITION_SIZE 3
#define LETTER 0
#define NUMBER 1


class Board;
class notFoundExeption;

class Position
{
public:
	Position();
	Position(const char pos[POSITION_SIZE]);
	Position(unsigned int x, unsigned int y);

	int getX();
	int getY();

	Position& operator=(const char pos[POSITION_SIZE]);
	void changePositionXY(unsigned int x, unsigned int y);
	Position& operator++();
	Position operator++(int);
	char& operator[](const int index);
	const char& operator[](const int index) const;
	bool operator==(const Position& right) ;

	char _position[3];
};


class outOfRangeExeption : public std::exception
{
public:
	virtual const char* what() const
	{
		return "out of range!";
	}
};


class Solider
{
public:
	Solider(Board& board);
	virtual ~Solider();
	
	virtual void move(Position& dest);
	virtual bool isMoveValid(Position dest) = 0;

	virtual char getType() = 0;
	Position& getPosition();
	bool getColor();

protected:
	Position _position;
	bool _color; // true = Black, false = White
	Board& _board;
};